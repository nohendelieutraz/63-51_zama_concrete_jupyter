# 📘 ZAMA Libraries Exploration with Jupyter Notebook

This repository provides a Docker Compose setup to explore ZAMA's `concrete` and `concrete-ml` ([more details](notebooks/concrete-ml/README.md)) libraries within a Jupyter Notebook environment. These libraries are part of ZAMA's suite for privacy-preserving computations, offering tools to build and deploy applications using Fully Homomorphic Encryption (FHE).

## 🎯 Purpose

The purpose of this repository is to facilitate the exploration and learning of ZAMA's `concrete` and `concrete-ml` ([more details](notebooks/concrete-ml/README.md)) libraries in an easy-to-use Jupyter Notebook setup. This will help users understand how to implement and utilize FHE in their machine learning or data analysis projects.

## 🔑 Understanding Fully Homomorphic Encryption (FHE)

Fully Homomorphic Encryption is a type of encryption that allows computation on ciphertexts, generating an encrypted result which, when decrypted, matches the result of operations performed on the plaintext.
This allows data to be processed securely without exposing the underlying data, enabling private computations in cloud environments and beyond.

```mermaid
sequenceDiagram
    participant C as Client
    participant S as Server

    Note left of C: Encrypts data
    C->>S: Send encrypted data
    Note right of S: Processes encrypted data using FHE operations
    S->>C: Return encrypted result
    Note left of C: Decrypts the result
```

## 🔗 Important Links

- [ZAMA Official Website](https://zama.ai/)
- [Concrete Library GitHub](https://github.com/zama-ai/concrete)
- [Concrete-ML Library GitHub](https://github.com/zama-ai/concrete-ml)

For more examples and usage of the ZAMA libraries in Rust, visit the following repository:

- [Rust Examples](https://gitlab.com/nohendelieutraz/63-51_zama_rust.git)

For more detail about the `concrete-ml` library, visit the following [README](notebooks/concrete-ml/README.md)


## 🐳 Using Docker to Launch the Jupyter Notebook

### Prerequisites

Before you begin, ensure you have Docker and Docker Compose installed on your machine. Visit the following links for installation guides:

- [Install Docker](https://docs.docker.com/get-docker/)
- [Install Docker Compose](https://docs.docker.com/compose/install/)

### Getting Started

1. **Clone the Repository**

```bash
git clone https://gitlab.com/nohendelieutraz/63-51_zama_concrete_jupyter.git
cd 63-51_zama_concrete_jupyter
```

2. **Launch Docker Compose**

Run the following command to build and start the Docker container which will automatically launch the Jupyter Notebook server.

```bash
docker-compose up
```

3. **Explore the Notebooks**

Once the container is running, can access the Jupyter Notebook server at http://localhost:8888/tree.

Navigate through the notebooks provided in the repository to explore the capabilities of the `concrete` and `concrete-ml` libraries.

## 🚀 Next Steps

As you familiarize yourself with the basic operations and functions, try modifying the examples or creating new notebooks to test out different features and functionalities of the libraries.

## 📝 License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## 👨‍💻 Authors

This project was developed by Thaddée, Nohen, and Benoit.