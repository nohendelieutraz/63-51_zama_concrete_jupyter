import time
import numpy as np
import pandas as pd
from sklearn.datasets import fetch_openml
from sklearn.model_selection import GridSearchCV, ShuffleSplit, train_test_split
from xgboost import XGBClassifier
from concrete.ml.sklearn import XGBClassifier as ConcreteXGBClassifier


def fetch_data():
    # Fetch data from openml and convert it to panda frame
    all_data = fetch_openml(data_id=40945, as_frame=True, cache=True).frame
    # Panda method to convert data type to appropriate data type
    all_data = all_data.convert_dtypes()
    # Convert column "survived" to number type
    all_data["survived"] = all_data["survived"].astype(np.number)

    return all_data


def prepare_data(all_data):
    # Divide data into train and test sets
    train_data, test_data = train_test_split(all_data, test_size=len(all_data.index) - 891)
    datasets = [train_data, test_data]

    # Drop irrelevant columns and create new features
    drop_columns = ["cabin", "boat", "body", "home.dest", "ticket"]

    for dataset in datasets:
        dataset.drop(drop_columns, axis=1, inplace=True)

    # Replace missing values by median values
    for dataset in datasets:
        # Complete missing Age values with median
        dataset.age.fillna(dataset.age.median(), inplace=True)
        # Complete missing Embarked values with the most common one
        dataset.embarked.fillna(dataset.embarked.mode()[0], inplace=True)
        # Complete missing Fare values with median
        dataset.fare.fillna(dataset.fare.median(), inplace=True)

    def get_bin_labels(bin_name, number_of_bins):
        labels = []
        for i in range(number_of_bins):
            labels.append(bin_name + f"_{i}")
        return labels

    # Add new features to help the model interpret some behaviors
    for dataset in datasets:
        # Emphasize on relatives
        dataset["FamilySize"] = dataset.sibsp + dataset.parch + 1
        dataset["IsAlone"] = 1
        dataset.IsAlone[dataset.FamilySize > 1] = 0

        # Consider an individual's social status
        dataset["Title"] = dataset.name.str.extract(r" ([A-Za-z]+)\.", expand=False)

        # Group fares and ages in "bins" or "buckets"
        # (Age are splitted in 4 equals portions to group age range)
        dataset["FareBin"] = pd.qcut(dataset.fare, 4, labels=get_bin_labels("FareBin", 4))
        dataset["AgeBin"] = pd.cut(dataset.age.astype(int), 5, labels=get_bin_labels("AgeBin", 5))

        # Remove now-irrelevant variables
        drop_column = ["name", "sibsp", "parch", "fare", "age"]
        dataset.drop(drop_column, axis=1, inplace=True)

    # Replace uncommon titles with "Rare"
    data = pd.concat(datasets)
    titles = data.Title.value_counts()
    uncommon_titles = titles[titles < 10].index
    for dataset in datasets:
        dataset.Title.replace(uncommon_titles, "Rare", inplace=True)

    # Convert categorical features to numerical
    categorical_features = train_data.select_dtypes(exclude=np.number).columns
    # Convert data of the selected columns to number type
    x_train = pd.get_dummies(train_data, columns=categorical_features, prefix=categorical_features)
    x_test = pd.get_dummies(test_data, columns=categorical_features, prefix=categorical_features)

    # Define target variables
    target = "survived"
    # Define target variables
    y_train = train_data[target]
    y_test = test_data[target]
    # Drop target column from x_train and x_test
    x_train = x_train.drop(columns=[target])
    x_test = x_test.drop(columns=[target])

    return x_train, y_train, x_test, y_test


def main():
    # Load data from openml (or from file system if already fetched)
    all_data = fetch_data()

    # Prepare data for machine learning
    x_train, y_train, x_test, y_test = prepare_data(all_data)

    # Instantiate the Cross-Validation generator
    cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
    # Define hyperparameters for GridSearchCV
    param_grid = {
        "max_depth": list(range(1, 5)),
        "n_estimators": list(range(1, 5)),
        "learning_rate": [0.01, 0.1, 1],
    }

    # ---------- XGBClassifier ----------
    # Train with 'XGBClassifier' and save the best model
    # Train the model
    start_time = time.time()
    model = GridSearchCV(XGBClassifier(), param_grid, cv=cv, scoring="roc_auc")
    model.fit(x_train, y_train)
    xgb_train_duration = time.time() - start_time
    print(f"XGBClassifier - Best hyperparameters: {model.best_params_}")
    print(f"XGBClassifier - Train Duration: {xgb_train_duration:.2f}s")

    # Predict
    start_time = time.time()
    xgb_predictions = model.predict(x_test)
    xgb_predict_duration = time.time() - start_time
    print(f"XGBClassifier - Predict Duration: {xgb_predict_duration:.2f}s")

    # Evaluate accuracy and ROC AUC
    xgb_predict_accuracy = np.mean(xgb_predictions == y_test) * 100
    print(f"XGBClassifier - Accuracy: {xgb_predict_accuracy:.2f}")

    # ---------- ConcreteXGBClassifier ----------
    # Train
    start_time = time.time()
    concrete_model = GridSearchCV(ConcreteXGBClassifier(), param_grid, cv=cv, scoring="roc_auc")
    concrete_model.fit(x_train, y_train)
    concrete_train_duration = time.time() - start_time
    print(f"ConcreteXGBClassifier - Best hyperparameters: {concrete_model.best_params_}")
    print(f"ConcreteXGBClassifier - Train Duration: {concrete_train_duration:.2f}s")

    # This line compiles the model so that it is able to run predictions using homomorphic encryption.
    # This line is necessary to run predictions in encrypted mode.
    # The subset of 100 samples is used to reduce compilation time.
    fhe_circuit = concrete_model.best_estimator_.compile(x_train.head(100))
    # This line generates the keys needed for data encryption and decryption.
    fhe_circuit.keygen()

    # Predict without encryption
    start_time = time.time()
    concrete_predictions_without_fhe = concrete_model.predict(x_test)
    concrete_predict_duration_without_fhe = time.time() - start_time
    print(f"ConcreteXGBClassifier - Predict Duration without encryption: {concrete_predict_duration_without_fhe:.2f}s")

    # Evaluate without encryption
    xgb_predict_accuracy_without_fhe = np.mean(concrete_predictions_without_fhe == y_test) * 100
    print(f"ConcreteXGBClassifier - Accuracy without encryption: {xgb_predict_accuracy_without_fhe:.2f}")

    # Predict with encryption
    start_time = time.time()
    # Using the fhe="execute" argument, this line indicates that predictions should be performed in encrypted mode.
    # The library automatically handles encryption of input data before sending it to the model, and the results are decrypted after calculation.
    concrete_predictions_with_fhe = concrete_model.best_estimator_.predict(x_test, fhe="execute")
    concrete_predict_duration_with_fhe = time.time() - start_time
    print(f"ConcreteXGBClassifier - Predict Duration with encryption: {concrete_predict_duration_with_fhe:.2f}s")

    # Evaluate with encryption
    xgb_predict_accuracy_with_fhe = np.mean(concrete_predictions_with_fhe == y_test) * 100
    print(f"ConcreteXGBClassifier - Accuracy without encryption: {xgb_predict_accuracy_with_fhe:.2f}")

    # Compare predictions
    number_of_equal_preds = np.sum(concrete_predictions_with_fhe == concrete_predictions_without_fhe)
    pred_similarity = number_of_equal_preds / len(concrete_predictions_without_fhe) * 100
    print(f"Prediction similarity between both Concrete ML models (quantized clear and FHE): {pred_similarity:.2f}%")

    number_of_equal_preds = np.sum(xgb_predictions == concrete_predictions_with_fhe)
    pred_similarity = number_of_equal_preds / len(concrete_predictions_with_fhe) * 100
    print(f"Prediction similarity between XGBClassifier and ConcreteXGBClassifier: {pred_similarity:.2f}%")

if __name__ == "__main__":
    main()
