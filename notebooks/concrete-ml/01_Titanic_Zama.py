import time
import numpy as np
import pandas as pd
from sklearn.datasets import fetch_openml
from sklearn.model_selection import GridSearchCV, ShuffleSplit, train_test_split
from xgboost import XGBClassifier
from concrete.ml.sklearn import XGBClassifier as ConcreteXGBClassifier


def main():
    # Load data from openml
    all_data = fetch_openml(data_id=40945, as_frame=True, cache=True).frame
    all_data = all_data.convert_dtypes()
    all_data["survived"] = all_data["survived"].astype(np.number)

    # Dataset contains 1309 lines of data
    # Split data into two sets (train set and test set)(train dataset => 891)(test set => 1309-891 = 418)
    train_data, test_data = train_test_split(all_data, test_size=len(all_data.index) - 891)
    # Create list of two datasets
    datasets = [train_data, test_data]

    # Print data (head() method from panda print the 5 first lines of the data frame)
    train_data.head()

    # Print columns where data are null, with the number of missing values
    print("---Train data---")
    print(train_data.isnull().sum(), "\n")
    print("---Test data---")
    print(test_data.isnull().sum())

    # Calculate percentage of missing value in each column
    for incomp_var in train_data.columns:
        missing_val = pd.concat(datasets)[incomp_var].isnull().sum()
        if missing_val > 0 and incomp_var != "survived":
            total_val = pd.concat(datasets).shape[0]
            print(f"Percentage of missing values in {incomp_var}: {missing_val / total_val * 100:.1f}%")

    drop_column = ["cabin", "boat", "body", "home.dest", "ticket"]

    # Remove irrelevant columns from datasets
    for dataset in datasets:
        dataset.drop(drop_column, axis=1, inplace=True)

    # Fill missing values
    for dataset in datasets:
        # Complete missing Age values with median
        dataset.age.fillna(dataset.age.median(), inplace=True)

        # Complete missing Embarked values with the most common one
        dataset.embarked.fillna(dataset.embarked.mode()[0], inplace=True)

        # Complete missing Fare values with median
        dataset.fare.fillna(dataset.fare.median(), inplace=True)

    # Print columns where data are null, with the number of missing values
    def get_bin_labels(bin_name, number_of_bins):
        labels = []
        for i in range(number_of_bins):
            labels.append(bin_name + f"_{i}")
        return labels

    # Add new columns to the datasets
    for dataset in datasets:
        # Emphasize on relatives
        dataset["FamilySize"] = dataset.sibsp + dataset.parch + 1

        dataset["IsAlone"] = 1
        dataset.IsAlone[dataset.FamilySize > 1] = 0

        # Consider an individual's social status
        dataset["Title"] = dataset.name.str.extract(r" ([A-Za-z]+)\.", expand=False)

        # Group fares and ages in "bins" or "buckets"
        # (Age are split in 4 equal portions to group age range)
        dataset["FareBin"] = pd.qcut(dataset.fare, 4, labels=get_bin_labels("FareBin", 4))
        dataset["AgeBin"] = pd.cut(dataset.age.astype(int), 5, labels=get_bin_labels("AgeBin", 5))

        # Remove now-irrelevant variables
        drop_column = ["name", "sibsp", "parch", "fare", "age"]
        dataset.drop(drop_column, axis=1, inplace=True)

    # Concat datasets (test data and train data)
    data = pd.concat(datasets)
    # Print title column values count
    titles = data.Title.value_counts()
    print(titles)

    # Get where title count is less than 10
    uncommon_titles = titles[titles < 10].index
    # Set title to "Rare" in the dataset
    for dataset in datasets:
        dataset.Title.replace(uncommon_titles, "Rare", inplace=True)

    # Select columns where data type is not a number
    categorical_features = train_data.select_dtypes(exclude=np.number).columns
    # Convert data of the selected columns to number type
    x_train = pd.get_dummies(train_data, columns=categorical_features, prefix=categorical_features)
    x_test = pd.get_dummies(test_data, columns=categorical_features, prefix=categorical_features)

    # Remove the column with "survived" value
    target = "survived"
    x_train = x_train.drop(columns=[target])
    y_train = train_data[target]

    x_test = x_test.drop(columns=[target])
    y_test = test_data[target]

    # Instantiate the Cross-Validation generator
    cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)

    # Set the parameters to tune.
    # Those ranges are voluntarily small in order to keep the FHE execution time per inference
    # relatively low. In fact, we found out that, in this particular Titanic example, models with
    # larger numbers of estimators or maximum depth don't score a much better accuracy.
    param_grid = {
        "max_depth": list(range(1, 5)),
        "n_estimators": list(range(1, 5)),
        "learning_rate": [0.01, 0.1, 1],
    }

    #-----XGBClassifier-----

    # Instantiate and fit the model through grid-search cross-validation
    time_begin = time.time()
    model = GridSearchCV(XGBClassifier(), param_grid, cv=cv, scoring="roc_auc")
    model.fit(x_train, y_train)
    cv_xgb_duration = time.time() - time_begin

    print(f"Best hyper-parameters found in {cv_xgb_duration:.2f}s :", model.best_params_)

    # The Concrete ML model needs an additional parameter used for quantization
    param_grid["n_bits"] = [2]

    # Instantiate and fit the model through grid-search cross-validation
    time_begin = time.time()
    concrete_model = GridSearchCV(ConcreteXGBClassifier(), param_grid, cv=cv, scoring="roc_auc")
    concrete_model.fit(x_train, y_train)
    cv_concrete_duration = time.time() - time_begin

    print(f"Best hyper-parameters found in {cv_concrete_duration:.2f}s :", concrete_model.best_params_)

    # Compute the predictions in clear using XGBoost
    clear_predictions = model.predict(x_test)

    # Compute the predictions in clear using Concrete ML
    clear_quantized_predictions = concrete_model.predict(x_test)

    # Compile the Concrete ML model on a subset
    fhe_circuit = concrete_model.best_estimator_.compile(x_train.head(100))

    #-----ConcreteXGBClassifier-----

    # Generate the keys
    # This step is not absolutely necessary, as keygen() is called, when necessary,
    # within the predict method.
    # However, it is useful to run it beforehand in order to be able to
    # measure the prediction executing time separately from the key generation one
    time_begin = time.time()
    fhe_circuit.keygen()
    key_generation_duration = time.time() - time_begin

    # Compute the predictions in FHE using Concrete ML
    time_begin = time.time()
    fhe_predictions = concrete_model.best_estimator_.predict(x_test, fhe="execute")
    prediction_duration = time.time() - time_begin

    print(f"Key generation time: {key_generation_duration:.2f}s")
    print(f"Total execution time for {len(clear_predictions)} inferences: {prediction_duration:.2f}s")
    print(f"Execution time per inference in FHE: {prediction_duration / len(clear_predictions):.2f}s")

    number_of_equal_preds = np.sum(fhe_predictions == clear_quantized_predictions)
    pred_similarity = number_of_equal_preds / len(clear_predictions) * 100
    print(
        "Prediction similarity between both Concrete ML models (quantized clear and FHE): "
        f"{pred_similarity:.2f}%",
    )

    accuracy_fhe = np.mean(fhe_predictions == y_test) * 100
    print(
        "Accuracy of prediction in FHE on the test set " f"{accuracy_fhe:.2f}%",
    )


if __name__ == "__main__":
    main()
