# 📘 Privacy-Preserving Machine Learning with Zama's Concrete ML

This directory contains code to demonstrates how to implement Privacy-Preserving Machine Learning (PPML) using the `concrete.ml.sklearn` library by [Zama](https://github.com/zama-ai/concrete-ml/tree/main), by using Fully Homomorphic Encryption (FHE).
The example used is based on the Titanic dataset from the [Kaggle competition](https://www.kaggle.com/c/titanic/).
The content of the Jupyter notebook and the python script is the same, use the one you prefer.

# 🎯 Purpose

The goal is to showcase the use of FHE to protect data privacy while using a machine learning model to predict outcomes.
The goal is also to do a comparison between standard machine learning models and FHE-protected models to evaluate the performance.
This is done without compromising the model's performance. We use an XGBoost classifier as our model, leveraging Zama's `concrete.ml.sklearn` library to perform predictions on encrypted data.


# 🚢 Titanic Use Case

The Titanic dataset is a classic dataset used for binary classification tasks.
The goal is to predict whether a passenger survived or not based on features like age, sex, ticket class, and fare.
There is three different implementations of this use case, explained below.

## 01_Titanic_Zama.ipynb
This notebook is a use case example from Zama's Concrete ML repository. It demonstrates how to use the `XGBClassifier` class from `concrete.ml.sklearn` library to perform machine learning using Fully Homomorphic Encryption (FHE).

This example is used to compare prediction time and similarity between predictions without using FHE and predictions using FHE.

🔗 [01_Titanic_Zama](01_Titanic_Zama.ipynb)

## 02_Titanic_Zama_Modified.ipynb
This notebook is a modified version of the Titanic notebook.

The goal is to compare performance of training and prediction time between standard machine learning models and FHE-protected models.

🔗 [02_Titanic_Zama_Modified](02_Titanic_Zama_Modified.ipynb)

## 03_Titanic.ipynb
This is also a modified version of the Titanic notebook but using `LogisticRegression` class from `concrete.ml.sklearn` instead of `XGBClassifier`.

The goal is to simulate a service provider that can train the model in order to later provide predictions to a client by receiving encrypted data.

```mermaid
sequenceDiagram
    participant Client
    participant Server

    Client->>Client: 1. Create dataset
    Client->>Server: 2. Send data for training
    Server->>Server: 3. Train ML model
    Server->>Client: 4. Send trained model
    Client->>Client: 5. Encrypt prediction data
    Client->>Server: 6. Send encrypted data for prediction
    Server->>Server: 7. Perform prediction on encrypted data
    Server->>Client: 8. Send encrypted results
    Client->>Client: 9. Decrypt results
``` 
🔗 [03_Titanic](03_Titanic.ipynb)

# Concrete ML classes used

Here is an explaination of the machine learning classes implemented.

## XGBClassifier (Extreme Gradient Boosting Classifier)
```python
from concrete.ml.sklearn import XGBClassifier as ConcreteXGBClassifier
```
- **Algorithm Type:** This is an implementation of the XGBoost (Extreme Gradient Boosting) algorithm, which is an ensemble learning method using gradient boosting techniques. 
- **Use Cases:** XGBoost is particularly well-suited for structured/tabular data and is known for its performance in competitions and practical applications. It can handle both classification and regression tasks.
- **Features:** XGBoost can automatically handle missing values, perform feature selection, and provide feature importance scores. It is highly configurable with numerous hyperparameters for tuning.
- **Complexity:** The model can capture complex patterns in the data due to its ensemble nature, combining multiple weak learners to form a strong learner.
- **FHE Support:** The ConcreteXGBClassifier version from the concrete.ml.sklearn module supports Fully Homomorphic Encryption, enabling secure predictions on encrypted data without exposing sensitive information.

## LogisticRegression
```python
from concrete.ml.sklearn import LogisticRegression
```
- **Algorithm Type:** This is an implementation of the logistic regression algorithm, which is a linear model used for binary classification tasks.
- **Use Cases:** Logistic regression is commonly used for binary classification problems where the goal is to predict one of two possible outcomes. It is widely used in medical statistics, credit scoring, and binary classification tasks in general.
- **Features:** Logistic regression is interpretable, providing coefficients that indicate the strength and direction of the relationship between each feature and the target variable. It can be extended to multi-class classification using techniques like one-vs-rest (OvR) or softmax regression.
- **Complexity:** The model is relatively simple and assumes a linear relationship between the input features and the log-odds of the target variable. It is less capable of capturing complex patterns compared to ensemble methods like XGBoost.
- **FHE Support:** The LogisticRegression version from the concrete.ml.sklearn module also supports Fully Homomorphic Encryption, allowing for secure predictions on encrypted data while maintaining privacy.

# ✅ Conclusion

This project illustrates how to use Zama's Concrete ML library to perform privacy-preserving machine learning using FHE.
By following this example, you can implement machine learning models that protect sensitive data while maintaining predictions accuracy.