import time
import numpy as np
import pandas as pd
import pickle
from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from concrete.ml.sklearn import LogisticRegression


class Predictor:

    def __init__(self, data):
        self.data = data
        self.X = None
        self.y = None
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.model = None
        self.fhe_circuit = None

    def prepare_data(self):
        self.X = self.data.data
        self.y = self.data.target

        # Drop irrelevant columns
        self.X.drop(columns=['ticket', 'cabin', 'boat', 'body', 'home.dest'], inplace=True)

        # Replace missing values by median values
        self.X['age'].fillna(self.X['age'].median(), inplace=True)
        self.X['fare'].fillna(self.X['fare'].median(), inplace=True)
        self.X['embarked'].fillna(self.X['embarked'].mode()[0], inplace=True)

        # Add new features to help the model interpret some behaviors
        self.X['family_size'] = self.X['sibsp'] + self.X['parch'] + 1
        self.X['is_alone'] = 1
        self.X.loc[self.X['family_size'] > 1, 'is_alone'] = 0
        self.X['title'] = self.X['name'].str.extract(r' ([A-Za-z]+)\.', expand=False)
        self.X['fare_bin'] = pd.qcut(self.X['fare'], 4, labels=['fare_bin_0', 'fare_bin_1', 'fare_bin_2', 'fare_bin_3'])
        self.X['age_bin'] = pd.cut(self.X['age'].astype(int), 5,
                                   labels=['age_bin_0', 'age_bin_1', 'age_bin_2', 'age_bin_3', 'age_bin_4'])

        # Drop irrelevant columns
        self.X.drop(columns=['name', 'sibsp', 'parch', 'fare', 'age'], inplace=True)

        # Replace unknown titles by 'Rare'
        self.X['title'] = self.X['title'].replace(
            ['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')

        # Convert categorical columns to binary indicators (one-hot encoding)
        self.X = pd.get_dummies(self.X, columns=['sex', 'embarked', 'pclass', 'title', 'fare_bin', 'age_bin'])

        # Encode the target column
        le = LabelEncoder()
        self.y = le.fit_transform(self.y)

        # Split data into training and test sets
        self.X_train, X_temp, self.y_train, y_temp = train_test_split(self.X, self.y, test_size=0.3, random_state=42)

        # Select only 50 rows for test data
        self.X_test = X_temp[:50]
        self.y_test = y_temp[:50]

        return self.X_train, self.X_test, self.y_train, self.y_test

    def train_and_return_model(self):
        # Instantiate and train the model
        self.model = LogisticRegression()
        self.model.fit(self.X_train, self.y_train)
        return self.model

    def compile(self):
        # Compile the model on a representative set
        self.fhe_circuit = self.model.compile(self.X_train)
        return self.fhe_circuit

    def predict_clear(self, X_test):
        # Clear predictions
        y_pred_clear = self.model.predict(X_test)
        return y_pred_clear

    def predict_fhe(self, encrypted_inputs):
        encrypted_predictions = []
        for i, q_input_enc in enumerate(encrypted_inputs):
            # Execute the linear product in FHE
            q_y_enc = self.fhe_circuit.run(q_input_enc)
            encrypted_predictions.append(q_y_enc)

        return encrypted_predictions


def save_model(model, model_path):
    # Save the model to disk
    with open(model_path, "wb") as f:
        pickle.dump(model, f)
    print("Model saved to disk.")


def load_model(model_path):
    # Load the model from disk
    with open(model_path, "rb") as f:
        model = pickle.load(f)
    print("Model loaded from disk.")
    return model


# Main script
if __name__ == "__main__":
    start_time = time.time()

    # Fetch data from openml
    data = fetch_openml(data_id=40945, as_frame=True, cache=True)

    # Create instance of FHEPredictor
    predictor = Predictor(data=data)

    # Prepare data and return training and test sets
    X_train, X_test, y_train, y_test = predictor.prepare_data()

    # Train and save the model
    model = predictor.train_and_return_model()

    # Compile the model on a representative set
    fhe_circuit = predictor.compile()

    # Display the first 5 rows of the training data
    print(X_train.head())

    start_time = time.time()
    # Predict with clear data
    y_pred_clear = predictor.predict_clear(X_test)

    # Display first test data
    print("Test data: ", X_test.values[0])
    # Display first predictions
    print("Clear preediction: ", y_pred_clear[0])
    # Display the execution time
    print(f"Execution time: {time.time() - start_time:.2f} seconds")

    start_time = time.time()
    # Quantize and encrypt the test inputs
    encrypted_inputs = [fhe_circuit.encrypt(model.quantize_input([f_input])) for f_input in X_test.values]

    # Predict with FHE
    encrypted_predictions = predictor.predict_fhe(encrypted_inputs)

    # Decrypt and dequantize predictions
    y_pred_fhe_step = [model.post_processing(model.dequantize_output(fhe_circuit.decrypt(q_y_enc))) for q_y_enc in
                       encrypted_predictions]
    # Convert to numpy array
    y_pred_fhe_step = np.array([np.argmax(y_proba) for y_proba in y_pred_fhe_step])

    # Display first encrypted data
    print("Encrypted data: ", encrypted_inputs[0])
    # Display first encrypted prediction
    print("Encrypted prediction: ", encrypted_predictions[0])
    # Display the execution time
    print(f"Execution time: {time.time() - start_time:.2f} seconds")

    # Display the similarity between the clear and FHE predictions
    print(f"Similarity between the clear and FHE predictions: {int((y_pred_fhe_step == y_pred_clear).mean() * 100)}%")

    # Calculate accuracy between y_test and y_pred_fhe_step
    accuracy_fhe = (y_pred_fhe_step == y_test).mean() * 100
    # Display accuracy of FHE predictions
    print(f"Accuracy of FHE predictions: {accuracy_fhe:.2f}%")

    # Create a DataFrame to display the results
    results_df = pd.DataFrame({
        "Valeur réelle": y_test,
        "Prédiction (clair)": y_pred_clear,
        "Prédiction (FHE)": y_pred_fhe_step
    })
    # Concatenate X_test with results_df for detailed view
    results_with_details = pd.concat([X_test.reset_index(drop=True), results_df], axis=1)
    # Display the detailed results
    print(results_with_details)
