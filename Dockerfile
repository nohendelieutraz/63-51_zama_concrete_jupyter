FROM zamafhe/concrete-ml:latest

RUN pip install matplotlib && pip install pygraphviz

WORKDIR /notebooks

CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root", "--NotebookApp.token=''"]
